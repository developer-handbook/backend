```bash
$ git clone https://gitlab.com/developer-handbook/backend.git
$ cd backend
```

```bash
$ npm install
```

```bash
$ npm run build
```

```bash
$ npm run test
```

```bash
$ npm run dev
```

## URL project start
This project frontend start docker `http://localhost:8000/api/v1`

```
backend
 └ src
    ├ @core
    │  ├ config
    │  │  └── ..
    │  ├ exceptions
    │  │  └── ..
    │  ├ interfaces
    │  │  └── ..
    │  ├ middlewares
    │  │  └── ..
    │  ├ repository
    │  │  └── ..
    │  └ utils
    │     └── ..
    ├ module
    │  └ module-name
    │    ├ dto
    │    │  └── ..
    │    ├ model
    │    │  └── ..
    │    ├ module-name.controller.rs
    │    ├ module-name.route.rs
    │    └ module-name.service.rs
    ├ app.ts
    ├ server.ts
    └ test
        └── ..
```