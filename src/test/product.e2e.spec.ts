import { Express } from 'express';
import mongoose from 'mongoose';
import request from 'supertest';
import { dbConnection } from '../@Core/repository';
import { ProductModel } from '../@Core/repository/product.schema';
import { App } from '../app';
import { CreateProductDto } from '../module/product/dto/CreateProductDto.dto';
import { Product } from '../module/product/model/product.interface';
import { ProductRoute } from '../module/product/product.route';

describe('E2E Test: Product Endpoints', () => {
  let app: Express;
  let newProduct: CreateProductDto = { name: "e-2-e aa", image: "e-2-e.png", stock: 10, price: 10 };
  let resultProduct: Product

  beforeAll(() => {
    dbConnection()
    const routes = [new ProductRoute()];
    app = new App(routes).getServer();
  });

  afterAll(async () => {
    await ProductModel.deleteMany({});
    await mongoose.connection.close();
  });

  it('GET /api/product should return list of products', async () => {
    const response = await request(app).get('/api/product');
    expect(response.status).toBe(200);
  });

  it('POST /api/product should create a new product', async () => {
    const response = await request(app)
      .post('/api/product')
      .send(newProduct);
    resultProduct = response.body?.data
    const { image, ...expectedWithoutImage } = newProduct; // Extract 'image' property from expectedProduct

    expect(response.status).toBe(201);
    expect(response.body.message).toEqual('create succeed');
    expect(response.body?.data).toEqual(expect.objectContaining(expectedWithoutImage));

  });

  it('GET /api/product/:id should a product by ID ', async () => {
    const response = await request(app)
      .get(`/api/product/${resultProduct._id.toString()}`)
      .expect(200);

    expect(response.body.message).toBe('find succeed');
    expect(response.body.data._id).toBe(resultProduct._id.toString());
  });

})