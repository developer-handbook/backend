import mongoose from 'mongoose';
import { DB_DATABASE, DB_HOST, DB_PASS, DB_PORT, DB_USER, NODE_ENV } from '../config';

const dbConnection = async (): Promise<void> => {
  const dbConfig = {
    url: `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}`,
    options: {
      dbName: DB_DATABASE,
      autoIndex: false,
      autoCreate: true
    },
  };
  mongoose.set('strictQuery', false);

  if (NODE_ENV !== 'production') {
    mongoose.set('debug', true);
  }
  await mongoose.connect(dbConfig.url, dbConfig.options);
};

export { dbConnection };

