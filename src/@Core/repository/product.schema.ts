import { Document, Schema, model } from "mongoose";
import { Product } from "../../module/product/model/product.interface";

const ProductSchema: Schema = new Schema({
    name: {
        type: String,
        default: null
    },
    image: {
        type: String,
        default: null
    },
    stock: {
        type: Number,
        default: null
    },
    price: {
        type: Number,
        default: null
    },
}, {
    timestamps: { 
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export const ProductModel = model<Product & Document>('Product', ProductSchema);