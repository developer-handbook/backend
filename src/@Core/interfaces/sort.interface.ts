import { SortOrder } from "mongoose";

export interface SortType {
  [key: string]: SortOrder | {
    $meta: "textScore";
  } 
}