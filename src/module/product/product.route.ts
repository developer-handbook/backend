import { Router } from "express";
import Container from "typedi";
import { Routes } from "../../@Core/interfaces/routes.interface";
import { ValidationMiddleware } from "../../@Core/middlewares/validation.middleware";
import { CreateProductDto } from "./dto/CreateProductDto.dto";
import { DeleteProductDto } from "./dto/DeleteProductDto.dto";
import { UpdateProductDto } from "./dto/UpdateProductDto.dto";
import { ProductController } from "./product.controller";

export class ProductRoute implements Routes {
    public path = '/product'
    public router = Router()
    private controller: ProductController = Container.get(ProductController)

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes () {
        this.router.get(`${this.path}`, this.controller.findAll);
        this.router.get(`${this.path}/:id`, this.controller.find);
        this.router.post(`${this.path}`, ValidationMiddleware(CreateProductDto), this.controller.create);
        this.router.patch(`${this.path}`, ValidationMiddleware(UpdateProductDto), this.controller.update);
        this.router.delete(`${this.path}`, ValidationMiddleware(DeleteProductDto), this.controller.delete);
    }
}
