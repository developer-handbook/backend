import fs from "fs";
import { Document } from "mongoose";
import path from "path";
import Container, { Service } from "typedi";
import { promisify } from 'util';
import { NotFoundException } from "../../@Core/exceptions/HttpException";
import { SortType } from '../../@Core/interfaces/sort.interface';
import { ProductModel } from '../../@Core/repository/product.schema';
import { CreateProductDto } from "./dto/CreateProductDto.dto";
import { DeleteProductDto } from "./dto/DeleteProductDto.dto";
import { UpdateProductDto } from "./dto/UpdateProductDto.dto";
import { Product } from "./model/product.interface";
const writeFileAsync = promisify(fs.writeFile);

@Service()
export class ProductService {
  
  public async findAll(skip: number, limit: number, searchWord: RegExp, sort: SortType): Promise<Product[]> {    
    const result: Product[] = await ProductModel.find({
      $or: [
        { name: searchWord }, 
        { image: searchWord } 
      ]
    }).skip(skip).limit(limit).sort(sort);
    return result;
  }
  
  public async countAll(): Promise<Product[]> { 
    const result: any = await ProductModel.count();
    return result;
  }

  public async find(id: string): Promise<Product> { 
    const result: Product = await ProductModel.findById(id);
    if (!result) {
      throw new NotFoundException('Product not found');
    }
    return result;
  }

  public async create(createProduct: CreateProductDto): Promise<Product> {
    const result: Product = await  ProductModel.create(createProduct);
    return result;
  }

  public async upload(image: string): Promise<string> {
    const base64Image = image

    const base64Data = base64Image.replace(/^data:image\/\w+;base64,/, '');

    const imageBuffer = Buffer.from(base64Data, 'base64');

    const directoryPath = 'uploads';
    const imageName = `image_${Date.now()}.jpeg`;
    const imagePath = path.join(directoryPath, imageName);

    if (!fs.existsSync(directoryPath)) {
      fs.mkdirSync(directoryPath, { recursive: true });
    }

    try {
      await writeFileAsync(imagePath, imageBuffer);
      console.log('Image saved successfully:', imageName);
      return imageName;
    } catch (err) {
      console.error('Error saving image:', err);
      return "error";
    }
  }

  public async update(productData: UpdateProductDto): Promise<Product> {
    const findProduct: Product & Document = await ProductModel.findById(productData._id)
    if (!findProduct) {
        throw new NotFoundException('Product not found')
    }
    findProduct.name = productData.name
    findProduct.stock = productData.stock
    findProduct.price = productData.price
    await findProduct.save()
    return findProduct
  }

  public async delete(deleteProduct: DeleteProductDto): Promise<Product> {
    const findProduct: Product & Document = await ProductModel.findById(deleteProduct._id)
    if (!findProduct) {
        throw new NotFoundException('Product not found')
    }
    await findProduct.delete()
    return findProduct
  }
  
}

Container.set(ProductService, new ProductService());