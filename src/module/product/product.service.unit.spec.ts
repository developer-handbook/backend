import { Container } from 'typedi';
import { ProductModel } from '../../@Core/repository/product.schema';
import { ProductService } from './product.service';

describe('ProductService', () => {
  let productService: ProductService;

  beforeAll(async () => {
    productService = Container.get(ProductService);
  });

  afterEach(() => {
    jest.restoreAllMocks(); // Reset all mocks after each test
  });

  it('finds a product by ID', async () => {
    const sampleProduct = { _id: '1', name: 'Sample Product' };
    jest.spyOn(ProductModel, 'findById').mockResolvedValue(sampleProduct);

    const productId = '1';

    const result = await productService.find(productId);

    expect(ProductModel.findById).toHaveBeenCalledWith(productId);
    expect(result).toEqual(sampleProduct);
  });
});