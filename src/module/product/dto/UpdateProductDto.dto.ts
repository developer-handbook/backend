import { IsNotEmpty, IsString } from "class-validator";
import { ProductDto } from "./ProductDto";

export class UpdateProductDto extends ProductDto {
    @IsString()
    @IsNotEmpty()
    readonly _id: string
}
