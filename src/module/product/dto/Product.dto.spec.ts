import { validate } from 'class-validator';
import { ProductDto } from './ProductDto';

function setPropertiesForTesting(dto: ProductDto, name: string, stock: number, price: number): void {
  (dto as any).name = name;
  (dto as any).stock = stock;
  (dto as any).price = price;
}

describe('Dto Validation', () => {
  it('ProductDto should pass validation', async () => {
    const validProduct = new ProductDto();
    setPropertiesForTesting(validProduct, 'Product Name', 10, 29.99);

    const validationErrors = await validate(validProduct);
    expect(validationErrors).toHaveLength(0);
  });

  it('ProductDto should fail when name is empty', async () => {
    const invalidProduct = new ProductDto();
    setPropertiesForTesting(invalidProduct, '', 10, 29.99);

    const validationErrors = await validate(invalidProduct);
    expect(validationErrors).toHaveLength(1);
  });

  it('ProductDto should fail when stock is not a number', async () => {
    const invalidProduct = new ProductDto();
    setPropertiesForTesting(invalidProduct, 'Product Name', NaN, 29.99);

    const validationErrors = await validate(invalidProduct);
    expect(validationErrors).toHaveLength(1);
  });
});