import { validate } from 'class-validator';
import { CreateProductDto } from './CreateProductDto.dto';
import { ProductDto } from './ProductDto';

function setPropertiesForTesting(dto: ProductDto, name: string, stock: number, price: number, image: string): void {
  (dto as any).name = name;
  (dto as any).stock = stock;
  (dto as any).price = price;
  (dto as any).image = image;
}

describe('Dto Validation', () => {
  it('should create a valid create product DTO', async () => {
    const validCreateProduct = new CreateProductDto();
    setPropertiesForTesting(validCreateProduct, 'Product Name', 10, 29.99, 'image.jpg');

    const validationErrors = await validate(validCreateProduct);
    expect(validationErrors).toHaveLength(0);
  });

  it('should fail when image is empty', async () => {
    const invalidCreateProduct = new CreateProductDto();
    setPropertiesForTesting(invalidCreateProduct, 'Product Name', 10, 29.99, '');

    const validationErrors = await validate(invalidCreateProduct);
    expect(validationErrors).toHaveLength(1);
  });
});