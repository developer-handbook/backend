import { IsNotEmpty, IsString } from "class-validator";
import { ProductDto } from "./ProductDto";

export class CreateProductDto extends ProductDto {

  @IsString()
  @IsNotEmpty()
  readonly image: string;
}