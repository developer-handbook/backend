import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class ProductDto {

  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsNumber()
  @IsNotEmpty()
  readonly stock: number;

  @IsNumber()
  @IsNotEmpty()
  readonly price: number;
}