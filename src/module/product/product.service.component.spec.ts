import mongoose, { SortOrder } from 'mongoose';
import { Container } from 'typedi';
import { NotFoundException } from '../../@Core/exceptions/HttpException';
import { SortType } from '../../@Core/interfaces/sort.interface';
import { dbConnection } from '../../@Core/repository';
import { ProductModel } from '../../@Core/repository/product.schema';
import { CreateProductDto } from './dto/CreateProductDto.dto';
import { Product } from './model/product.interface';
import { ProductService } from './product.service';

describe('ProductService', () => {
  let productService: ProductService;
  let createProduct: Product;

  beforeAll(async () => {
    dbConnection()
    productService = Container.get(ProductService);
  });

  afterAll(async () => {
    await ProductModel.deleteMany({});
    await mongoose.connection.close();
  });

  afterEach(() => {
    jest.restoreAllMocks(); // Reset all mocks after each test
  });

  it('should get all products', async () => {
    const keyword: string = '';
    const sortKey: string = 'created_at';
    const sortValue: SortOrder = 'desc';
    const sort: SortType = {
      [sortKey]: sortValue
    }
    const searchWord: RegExp = new RegExp(keyword, 'i');

    const allProducts = await productService.findAll(0, 10, searchWord, sort);
    expect(allProducts).toEqual([]);
  });

  it('finds a product by ID', async () => {
    const sampleProduct = { _id: '1', name: 'Sample Product' };
    jest.spyOn(ProductModel, 'findById').mockResolvedValue(sampleProduct);

    const productId = '1';

    const result = await productService.find(productId);

    expect(ProductModel.findById).toHaveBeenCalledWith(productId);
    expect(result).toEqual(sampleProduct);
  });

  it('should create a post', async () => {
    const createProductDto: CreateProductDto = { name: "aa", image: "bb", stock: 10, price: 10 };
    const result: Product = await productService.create(createProductDto);
    createProduct = (result as any).toObject({ getters: true });
    expect(result).toBeDefined();
  });

  it('should get one product', async () => {
    const product: Product | Document = await productService.find(createProduct._id);
    const productTemp = (product as any).toObject({ getters: true });
    expect(productTemp).toEqual(createProduct);
  });

  it('throws NotFoundException when product is not found in findOne', async () => {
    const nonExistentProductId = '655ceb8178bd383ce141f737';
    try {
      await productService.find(nonExistentProductId);
      fail('Expected NotFoundException was not thrown');
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException);
      expect(error.message).toBe('Product not found');
    }
  });

  it('throws NotFoundException when ObjectId format is incorrect', async () => {
    const incorrectObjectId = '655ceb8178bd383ce141f';
    try {
      await productService.find(incorrectObjectId);
      fail('Expected NotFoundException was not thrown');
    } catch (error) {
      expect(error.name).toBe('CastError');
      expect(error.message).toContain('Cast to ObjectId failed');
    }
  });
});