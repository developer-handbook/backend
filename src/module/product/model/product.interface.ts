export interface Product {
  _id: string
  name: string
  image: string
  stock: number
  price: number
}