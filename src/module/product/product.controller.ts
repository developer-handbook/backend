import { NextFunction, Request, Response } from "express";
import { SortOrder } from "mongoose";
import Container, { Service } from "typedi";
import { SortType } from "../../@Core/interfaces/sort.interface";
import { CreateProductDto } from "./dto/CreateProductDto.dto";
import { DeleteProductDto } from "./dto/DeleteProductDto.dto";
import { UpdateProductDto } from "./dto/UpdateProductDto.dto";
import { Product } from "./model/product.interface";
import { ProductService } from "./product.service";

@Service()
export class ProductController {
  private service: ProductService = Container.get(ProductService)
  private PAGE_SIZE: number = 10;
  private CURRENT_PAGE: number = 10;

  public findAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const currentPage = req.query?.currentPage || this.CURRENT_PAGE;
      const pageSize = req.query?.pageSize || this.PAGE_SIZE;
      const skip = (currentPage - 1) * pageSize;
      const keyword: string = req.query?.q;
      const sortKey: string = req.query?.sortKey || 'created_at';
      const sortValue: SortOrder = req.query?.sortValue || 'asc';
      const sort: SortType = {
        [sortKey]: sortValue
      }
      const searchWord: RegExp = new RegExp(keyword, 'i');

      const product: Product[] = await this.service.findAll(skip, pageSize, searchWord, sort);
      const countProduct: Product[] = await this.service.countAll();

      res.status(200).json({
        message: 'findAll succeed',
        data: product,
        currentPage: currentPage,
        pageSize: this.PAGE_SIZE,
        total: countProduct
      });
    } catch (error) {
      next(error);
    }
  };

  public find = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const productId: string = req.params.id;
      const product: Product = await this.service.find(productId);
      res.status(200).json({ message: 'find succeed', data: product });
    } catch (error) {
      next(error);
    }
  };

  public create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const createProduct: CreateProductDto = { ...req.body };
      const imageName: string = await this.service.upload(req.body.image)

      const result: Product = await this.service.create({ ...createProduct, image: imageName });
      res.status(201).json({ message: 'create succeed', data: result });
    } catch (error) {
      next(error);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const createProduct: UpdateProductDto = { ...req.body };
      const result: Product = await this.service.update(createProduct);
      res.status(200).json({ message: 'update succeed', data: result });
    } catch (error) {
      next(error);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const deleteProduct: DeleteProductDto = { ...req.body };
      const result: Product = await this.service.delete(deleteProduct);
      res.status(200).json({ message: 'delete succeed', data: result });
    } catch (error) {
      next(error);
    }
  };

}

