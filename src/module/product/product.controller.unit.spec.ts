import { ProductController } from './product.controller';

describe('ProductService', () => {
  let productServiceMock;
  let productController;

  beforeEach(() => {
    productServiceMock = {
      findAll: jest.fn(),
      countAll: jest.fn()
    };
    productController = new ProductController();
    productController.service = productServiceMock;
  });

  it('should return products when findAll is called', async () => {
    const mockRequest = {
      query: { q: 'searchKeyword', sortKey: 'name', sortValue: 'asc' }
    };
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn()
    };
    const mockNext = jest.fn();

    productServiceMock.findAll.mockResolvedValueOnce([
      {
        _id: '6560546feaf8df772b877cf1',
        name: "string",
        image: "string",
        stock: 5,
        price: 5
      }
    ]);
    productServiceMock.countAll.mockResolvedValueOnce(10);

    await productController.findAll(mockRequest, mockResponse, mockNext);

    expect(productServiceMock.findAll).toHaveBeenCalledWith(0, 10, expect.any(RegExp), { name: 'asc' });
    expect(mockResponse.status).toHaveBeenCalledWith(200);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: 'findAll succeed',
      data: expect.any(Array),
      pageNumber: 1,
      pageSize: 10,
      total: 10
    });
    expect(mockNext).not.toHaveBeenCalled();
  });
});