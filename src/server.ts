import { ValidateEnv } from './@Core/utils/validateEnv';
import { App } from './app';
import { ProductRoute } from './module/product/product.route';
ValidateEnv();

const routes = [
  new ProductRoute(),
];

const app = new App(routes);

app.listen();
